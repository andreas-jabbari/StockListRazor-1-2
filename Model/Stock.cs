﻿using System.ComponentModel.DataAnnotations;

namespace StockListRazor_1._2.Model
{
    public class Stock
    {
        [Key]                               //adds Id as an identiy column so we dont have to pass the value
        public int Id { get; set; }

        [Required]  
        public string Ticker { get; set; }

        public int Price { get; set; }

        public int Ammount { get; set; }
}
}
