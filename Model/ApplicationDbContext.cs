﻿using Microsoft.EntityFrameworkCore;

namespace StockListRazor_1._2.Model
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)             //this parameter is needed for dependacy injection
        {

        }

        public DbSet<Stock> Stock { get; set; }
    }                                          
}
