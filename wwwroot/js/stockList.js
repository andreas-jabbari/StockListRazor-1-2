﻿var dataTable;

$(document).ready(function(){
    loadDataTable();
})

function loadDataTable() {
    dataTable = $('#DT-load').DataTable({
        "ajax": {
            "url": "/api/stock",
            "type": "GET",
            "datatype": "json",
        }
        "columns": [
            { "data": "ticker", "width": "20%" },               //fix formatting
            { "data": "price", "width": "20%" },
            { "data": "ammount", "width": "20%" },
            {
                "data": "id",
                "render": function (data) {                     //has id, needed for function button
                    return `<div class="text-center">
                        <a href="/StockList/Edit?id=${data}" class='btn btn-success text-white' style='cursor:pointer; width:70px;'>
                            Edit
                        </a>
                        &nbsp;
                        <a class='btn btn-danger text-white' style='cursor:pointer; width:70px;'
                            onclick=Delete('/api/stock?id='+${data})>
                            Delete
                        </a>
                        </div>`;
                }, "width": "30%"
            }
        ],
        "language": {
            "emptyTable": "no data found"
        },
        "width": "100%"
    });
}