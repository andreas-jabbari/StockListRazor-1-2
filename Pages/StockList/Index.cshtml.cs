using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using StockListRazor_1._2.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StockListRazor_1._2.Pages.StockList
{
    public class IndexModel : PageModel
    {

        private readonly ApplicationDbContext _db;


        public IndexModel(ApplicationDbContext db)              //using dependancy injection
        {
            _db = db;                                           //can pull from container and inject into page
        }

        public IEnumerable<Stock> Stocks { get; set; }


        public async Task OnGet()       
        {
            Stocks = await _db.Stock.ToListAsync();
        }

        public async Task<IActionResult> OnPostDelete(int id)
        {
            var stock = await _db.Stock.FindAsync(id);
            if(stock == null)
            {
                return NotFound();
            }
            _db.Stock.Remove(stock);
            await _db.SaveChangesAsync();

            return RedirectToPage("Index");   

        }
               
    }
}
