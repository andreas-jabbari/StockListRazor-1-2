using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using StockListRazor_1._2.Model;
using System.Threading.Tasks;

namespace StockListRazor_1._2.Pages.StockList
{
    public class EditModel : PageModel
    {
        private ApplicationDbContext _db;

        public EditModel(ApplicationDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Stock Stock { get; set; }
        public object StockFromDb { get; private set; }

        public async Task OnGet(int id)
        {
            Stock = await _db.Stock.FindAsync(id);
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var StockFromDb = await _db.Stock.FindAsync(Stock.Id);
                StockFromDb.Ticker = Stock.Ticker;
                StockFromDb.Price = Stock.Price;
                StockFromDb.Ammount = Stock.Ammount;

                await _db.SaveChangesAsync();

                return RedirectToPage("Index");               //fix redirect
            }
            return RedirectToPage();
        }
    }
}
