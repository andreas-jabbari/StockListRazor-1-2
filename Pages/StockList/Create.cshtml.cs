using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using StockListRazor_1._2.Model;
using System.Threading.Tasks;

namespace StockListRazor_1._2.Pages.StockList
{
    public class CreateModel : PageModel
    {
        private readonly ApplicationDbContext _db;

        public CreateModel(ApplicationDbContext db)
        {
            _db= db;
        }

        [BindProperty]
        public Stock Stock { get; set; }                //passes the empty stock automatically, dont need to do it seperately

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)                             //server side validation
            {
                await _db.Stock.AddAsync(Stock);                //adds stock to que
                await _db.SaveChangesAsync();                   //when this is executed, data is pushed to database
                return RedirectToPage("Index");                                 
            }
            else
            {
                return Page();
            }
        }
    }
}
