﻿using Microsoft.AspNetCore.Mvc;
using StockListRazor_1._2.Model;
using System.Linq;

namespace StockListRazor_1._2.Controllers
{
    [Route("api/Stock")]
    [ApiController]
    public class StockController : Controller
    {
        private readonly ApplicationDbContext _db;

        public StockController(ApplicationDbContext db)
        {
            _db = db;   
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Json(new { data= _db.Stock.ToList() });                  //this api call should return the book list
        }
    }
}
