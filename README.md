# Stock List App

An app designed to take stock info that I parse and store
them in a database, then show them from the database onto
a table, and allow the data to be manipulated.




## Authors

- Andreas Jabbari (https://gitlab.com/andreas-jabbari)


## Features

- add data
- show data
- edit data
- delete data

Pipeline works

## features to come

- sorting
- search function
- second table with filters (WIP)



## How to use

Download and run the project, then navigate to the stocks page

```bash
  In the stocks page, click "add new stock" and you
  will be redirected to a page where you must enter ticker,
  price, and ammount.

  if you do not fill the fields, it will complain.

  After pressing add, they will be added to the table and
  database.

  There you can edit or delete stock data how you wish.
```
    
